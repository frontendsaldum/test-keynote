# #Git for beginners

**Ideas**:

- git stash /save/pop/drop/clear (*fragments*)
- sdsd
- git push *origin branch*
- git push *-u* origin new_branch
- delete local and remote branches. How and when
- git fetch
- basic concepts and philosophy
- basic commands
- what is a commit, a branch, merge === add changes
- how to work with git: Jira stories == git branches
- how to name branches
- undone changes: *git checkout*
- undone stagged changes *git reset HEAD*
- undone commits *git reset HEAD~*
- undone remote commits: first in local, later push (maybe with --force)
- git reset HEAD~*n*
- what is *HEAD*


asd
- why sourcetree is bad: at the begining
- to use or not to use git aliases
- *git init* and *git clone*. Differences
- *git status* and *git status -s*
- *git diff* in stagged changes (*git diff --staged*) or between branches
- *git add*, *git add .*
- *git commit -am*
- git merge. 
- Commit merge with conflics
- *git MERGE*
- .gitignore
- git *log*
- git bisect: just it exists
- *git branch* to list branches. *git branch -r* to remote
- ===
- how to deploy  
- git merge with conflicts
- git push branch + git delete local branch + git pull
- references: official docu, courses, stackoverflow
- ===
- I have my own full copy of the repo. I need to keep my local repository in sync with the remote repository by getting the latest commits from the remote repository



===========

#Recomendations:
* iTerm2 + plugins + natural writing
* SourceTree is evil (with beginners -in spanish is worst-)
* Nano its easier
* Use terminal

#First considerations
* Trust in Git. It ~~loves~~ helps you
* The pilosophy is important. Obey.

# Git as a version system?
> Help you *track changes* you make in your code *over time*. As you edit to your code, you tell the version control system to *take a snapshot of your files*. The version control system saves that snapshot permanently so you can recall it later if you need it.

> -Visual code documentation

asdasdasdasda


Good: 

* Create workflows: select what deploy
* Works with versions
* the team code together: manage conflics, allow members change the same file(s)
* Keep a history
* Help with task automatization
* Overview: Allow scale projects

Bad:

* You must learn git ;D


![](https://cdn-images-1.medium.com/max/1600/1*bI7M7HClmLLco_2SG_yg0g.png)

# Git basics

Git helps you to **dev and mantain a version** of your code. Also, it helps you to **revert changes** over the time and **find errors made long time ago*.

**To be a tema is not a requeriment**. You can work alone. 

Local Repository:



# `$ git add + git commit`

**SNAPSHOT concept**

git add .
git add file_path

git commit -m
git commit -am

# git merge 
# git 




![](https://blog.juanluisgarciaborrego.com/wp-content/uploads/2015/10/index1@2x.png)


-----


# Make a release:

* feature branches must not be merged into develop
* in develop will be the branches which can be deployed

	$ git checkout master	
	$ git pull 
	$ git checkout release	
	$ git pull    
	$ git merge master
	$ theme download -e production warning   
	$ git merge develop or feaute-branch   

	... TEST THE VERSION ...	
	$ theme upload -e DEV-PRODUCTION   
	... END TEST THE VERSION ...	

	$ theme upload -e PRODUCTION	
	$ git push origin release	
	$ git checkout master	
	$ git merge release	
	$ git push origin master	
	$ git checkout develop	
	$ git merge master
	

# References:
* Git flow: http://nvie.com/posts/a-successful-git-branching-model/



qweqwe